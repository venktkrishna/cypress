import React from 'react';

import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

import { ErrorPage } from '@pages';

import { ProtectedRoute } from '@scaffolding';

import {
  VirtualTraining,
  SignUp,
  Login,
  Sidebar,
  Confirmation,
  MemberPtPurchase,
  NonPtPayment,
  PtPayment,
  NonMemberPurchase,
  NonMemberContact,
  NonMemberPayment,
  NonPTPurchase
} from '@components';

const App = () => (
  <Router>
    <Switch>
      <ProtectedRoute path="/" exact component={VirtualTraining} />
      <ProtectedRoute path="/sidebar" exact component={Sidebar} />
      <ProtectedRoute path="/virtualTraining" exact component={VirtualTraining} />
      <ProtectedRoute path="/login" exact component={Login} />
      <ProtectedRoute path="/signUp" exact component={SignUp} />
      <ProtectedRoute path="/confirmation" exact component={Confirmation} />
      <ProtectedRoute path="/memberPtPurchase" exact component={MemberPtPurchase} />
      <ProtectedRoute path="/nonPtPayment" exact component={NonPtPayment} />
      <ProtectedRoute path="/nonPTPurchase" exact component={NonPTPurchase} />
      <ProtectedRoute path="/ptPayment" exact component={PtPayment} />
      <ProtectedRoute path="/nonMemberPurchase" exact component={NonMemberPurchase} />
      <ProtectedRoute path="/nonMemberContact" exact component={NonMemberContact} />
      <ProtectedRoute path="/nonMemberPayment" exact component={NonMemberPayment} />
      <Route component={ErrorPage} />
    </Switch>
  </Router>
);

export default App;
