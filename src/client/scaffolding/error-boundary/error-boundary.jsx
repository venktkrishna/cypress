import React from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError() {
    return { hasError: true };
  }

  render() {
    const { hasError } = this.state;
    const { children } = this.props;

    if (hasError) {
      return (
        <div>
          <h1>encountered an error...</h1>
        </div>
      );
    }

    return children;
  }
}

ErrorBoundary.defaultProps = {
  children: null
};

ErrorBoundary.propTypes = {
  children: PropTypes.node
};

export default ErrorBoundary;
