import React from 'react';
import PropTypes from 'prop-types';
import BasePage from '@BasePage';

export default function ProtectedRoute(
  { component, ...props }
) {
  const Page = component;
  return (
    <BasePage includeHeader={false}>
      <Page {...props} />
    </BasePage>
  );
}

ProtectedRoute.propTypes = {
  component: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.element,
    PropTypes.func,
    PropTypes.node]).isRequired
};
