import thunk from 'redux-thunk';
import configureMockStore from 'redux-mock-store';
import configureStore from '../store';

export const mockStoreForAction = ({ async } = {}) => {
  const middlewares = async ? [thunk] : [];
  return configureMockStore(middlewares)();
};

export const store = configureStore();
