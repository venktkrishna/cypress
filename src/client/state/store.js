import {
  createStore,
  applyMiddleware,
  compose,
  combineReducers
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { __DEV__, REDUX_LOGGER_ENABLED } from '@config/app-config';
import * as reducers from './redux';

/**
 * Redux Logger: https://github.com/LogRocket/redux-logger
 * Redux Devtools Extension: https://github.com/zalmoxisus/redux-devtools-extension/
 */

export default function configureStore() {
  const rootReducer = combineReducers(reducers);

  const middlewares = [thunkMiddleware];
  const middlewareEnhancer = applyMiddleware(...middlewares);
  /* eslint-disable-next-line no-underscore-dangle */
  const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const enhancers = composeEnhancer(middlewareEnhancer);

  if (__DEV__ && REDUX_LOGGER_ENABLED) {
    const logger = createLogger({ duration: true });
    middlewares.push(logger);
  }

  const store = createStore(rootReducer, enhancers);

  if (__DEV__ && module.hot) {
    module.hot.accept('./redux', () => store.replaceReducer(rootReducer));
  }

  return store;
}
