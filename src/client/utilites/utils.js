const setUrl = (domain) => {
  const { origin } = window.location;
  if (origin.includes('int')
    || origin.includes('test')
    || origin.includes('qa')
    || origin.includes('localhost')) {
    return `https://qa-${domain}`;
  } if (origin.includes('stag')) {
    return `https://stag-${domain}`;
  } if (origin.includes('preprod')) {
    return `https://preprod-${domain}`;
  }
  return `https://${domain}`;
};

export default setUrl;
