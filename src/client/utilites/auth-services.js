import axios from 'axios';
import qs from 'querystring';
import { Cookies } from 'react-cookie';
import setUrl from './utils';

const jwt = require('jsonwebtoken');


const AuthService = {
  name: 'AuthUtils',
  getLoginRedirectUrl() {
    return `${setUrl('login.equinox.com')}/Account/Login?ReturnUrl=/connect/authorize/callback?client_id=amexvpt%26scope=email%20openid%20profile%26redirect_uri=${window.location.origin}%26response_type=code`;
  },
  async getToken() {
    const urlParams = new URLSearchParams(window.location.search);
    let authToken = '';
    if (urlParams.get('code')) {
      const bodyObject = {
        client_id: 'amexvpt',
        scope: 'email openid profile',
        redirect_uri: `${window.location.origin}`,
        code: urlParams.get('code'),
        grant_type: 'authorization_code'
      };
      authToken = await axios({
        method: 'POST',
        url: `${setUrl('login.equinox.com')}/connect/token`,
        data: qs.stringify(bodyObject),
        headers: {
          'Access-Control-Allow-Origin': window.location.origin,
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      });
    }
    return authToken;
  },
  getAuthToken() {
    const { origin } = window.location;
    if (origin.includes('int')
      || origin.includes('test')
      || origin.includes('qa')
      || origin.includes('localhost')) {
      return 'Bearer eyJraWQiOiJTOEUzU1kvQzVyYndDTmxPN3VxdUppa2hFUFFNOHVXMzN3aWhaTW80L1hzPSIsImFsZyI6IkhTNTEyIn0.eyJzdWIiOiJjYmI1Nzg0OS1mMDQ3LTRhNTAtODZjYS01OWI1NTVjY2ZkYTUiLCJhdWQiOiJuZzkxZ3RvZGk5NTI5ZWpzaDkxajc4MmllIiwiZXZlbnRfaWQiOiI1MWM0NjA3OS0zMjYzLTRkMDQtOTlhYy0zYmQ2NzBhY2U5ZWEiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU2MDgzMTYzMSwiaXNzIjoiaHR0cHM6Ly9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbS91cy13ZXN0LTJfbkRJdVlrWG9xIiwiY29nbml0bzp1c2VybmFtZSI6InRyYWluaW5nLWNsaWVudCIsImV4cCI6MTU2MDgzNTIzMSwiaWF0IjoxNTYwODMxNjMxfQ.lydzGEQwGPOzwvOu4vSGXHearuJlqJFbzFqY3Fyc45Q5qp2YpYILrkN66NJoz3v4wShpHyEoSr-uFFJQN0sINA';
    } if (origin.includes('stag')) {
      return 'Bearer eyJraWQiOiJTOEUzU1kvQzVyYndDTmxPN3VxdUppa2hFUFFNOHVXMzN3aWhaTW80L1hzPSIsImFkhTNTEyIn0.eyJzdWIiOiJjYmI1Nzg0OS1mMDQ3LTRhNTAtODZjYS01OWI1NTVjY2ZkYTUiLCJhdWQiOiJuZzkxZ3RvZGk5NTI5ZWpzaDkxajc4MmllIiwiZXZlbnRfaWQiOiI1MWM0NjA3OS0zMjYzLTRkMDQtOTlhYy0zYmQ2NzBhY2U5ZWEiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU2MDgzMTYzMSwiaXNzIjoiaHR0cHM6Ly9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbS91cy13ZXN0LTJfbkRJdVlrWG9xIiwiY29nbml0bzp1c2VybmFtZSI6InRyYWluaW5nLWNsaWVudCIsImV4cCI6MTU2MDgzNTIzMSwiaWF0IjoxNTYwODMxNjMxfQ.lydzGEQwGPOzwvOu4vSGXHearuJlqJFbzFqY3Fyc45Q5qp2YpYILrkN66NJoz3v4wShpHyEoSr-uFFJQN0sINA';
    } if (origin.includes('preprod')) {
      return 'Bearer eyJraWQiOiJTOEUzU1kvQzVyYndDTmxPN3VxdUppa2hFUFFNOHVXMzN3aWhaTW80L1hzPSIsImFsZyI6IkhTNTEyIn0.eyJzdWIiOiJjYmI1Nzg0OS1mMDQ3LTRhNTAtODZjYS01OWI1NTVjY2ZkYTUiLCJhdWQiOiJuZzkxZ3RvZGk5NTI5ZWpzaDkxajc4MmllIiwiZXZlbnRfaWQiOiI1MWM0NjA3OS0zMjYzLTRkMDQtOTlhYy0zYmQ2NzBhY2U5ZWEiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU2MDgzMTYzMSwiaXNzIjoiaHR0cHM6Ly9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbS91cy13ZXN0LTJfbkRJdVlrWG9xIiwiY29nbml0bzp1c2VybmFtZSI6InRyYWluaW5nLWNsaWVudCIsImV4cCI6MTU2MDgzNTIzMSwiaWF0IjoxNTYwODMxNjMxfQ.lydzGEQwGPOzwvOu4vSGXHearuJlqJFbzFqY3Fyc45Q5qp2YpYILrkN66NJoz3v4wShpHyEoSr-uFFJQN0sINA';
    }
    return 'Bearer eyJraWQiOiJTOEUzU1kvQzVyYndDTmxPN3VxdUppa2hFUFFNOHVXMzN3aWhaTW80L1hzPSIsImFsZyI6IkhTNTEyIn0.eyJzdWIiOiJjYmI1Nzg0OS1mMDQ3LTRhNTAtODZjYS01OWI1NTVjY2ZkYTUiLCJhdWQiOiJuZzkxZ3RvZGk5NTI5ZWpzaDkxajc4MmllIiwiZXZlbnRfaWQiOiI1MWM0NjA3OS0zMjYzLTRkMDQtOTlhYy0zYmQ2NzBhY2U5ZWEiLCJ0b2tlbl91c2UiOiJpZCIsImF1dGhfdGltZSI6MTU2MDgzMTYzMSwiaXNzIjoiaHR0cHM6Ly9jb2duaXRvLWlkcC51cy13ZXN0LTIuYW1hem9uYXdzLmNvbS91cy13ZXN0LTJfbkRJdVlrWG9xIiwiY29nbml0bzp1c2VybmFtZSI6InRyYWluaW5nLWNsaWVudCIsImV4cCI6MTU2MDgzNTIzMSwiaWF0IjoxNTYwODMxNjMxfQ.lydzGEQwGPOzwvOu4vSGXHearuJlqJFbzFqY3Fyc4pYILrkN66NJoz3v4wShpHyEoSr-uFFJQN0sINA';
  },
  async getMemberDetails(memberId) {
    const memberDetails = await axios({
      method: 'GET',
      url: `${setUrl('gateway.equinox.com/pt/member/memberDetails')}/${memberId}`,
      headers: {
        'Access-Control-Allow-Origin': window.location.origin,
        authorizationToken: this.getAuthToken()
      }
    });
    return memberDetails;
  },
  async setCookie() {
    let token;
    try {
      token = await this.getToken();
      if (token && token.data) {
        const memberId = jwt.decode(token.data.id_token).mosoid;
        const memberDetails = await this.getMemberDetails(memberId);
        this.redirectToPaymentApp(memberDetails.data, memberId);
      }
    } catch (err) {
      console.log(err);
    }
    return token;
  },
  redirectToPaymentApp(data, memberId) {
    const memberInfo = data && data.memberDetails;
    const cookieOptions = {
      path: '/',
      maxAge: 600,
      domain: window.location.origin.includes('localhost') ? 'localhost' : '.equinox.com',
      secure: !window.location.origin.includes('localhost')
    };
    new Cookies().set(
      'referrer',
      `${window.location.origin}/${window.location.path}`,
      cookieOptions
    );
    new Cookies().set('amexData', memberInfo, cookieOptions);
    new Cookies().set('memberId', memberId, cookieOptions);
    window.location.href = window.location.origin.includes('localhost') ? 'http://localhost:30001' : setUrl('payment.equinox.com');
  }
};

export default AuthService;
