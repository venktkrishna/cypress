import React, { Component } from 'react';
import Header from '../header/header';

class login extends Component {
  state = {}

  render() {
    return (
      <section className="login-section">
        <Header title="Get Results. Wherever, Whenever." screenNumber={2} />
        <div className="custom-padding">
          <div className="vt-text">
            {'Sign in with your email to purchase sessions and start training.'}
          </div>
          <div>
            <form>
              <div className="row">
                <div className="col-lg-5">
                  <input type="email" className="input-style" id="exampleInputEmail1" placeholder="Email" />
                  <input type="password" className="input-style" id="exampleInputEmail1" placeholder="Password" />
                </div>
              </div>
            </form>
          </div>

          <button type="button" className="btn-next"><a className="btn-link" href="/#">Sign In</a></button>
          <a className="forget-password" href="/#">Forgot Email or Password</a>
          <div className="vt-footer">
            <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
            <p className="footer-text">
              <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
              <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
            </p>
            <p className="footer-text mb-0">
              <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
              <span className="ml-3">Member Policies</span>
            </p>
          </div>

        </div>
      </section>
    );
  }
}

export default login;
