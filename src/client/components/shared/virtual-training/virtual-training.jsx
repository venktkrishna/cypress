import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../header/header';
import Loader from '../loader/index';
import AuthService from '../../../utilites/auth-services';
import Footer from '../footer/footer';

class VirtualTraining extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };
  }

  componentDidMount() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.get('code')) {
      this.setState({ isLoading: true });
    }

    AuthService.setCookie();
  }

  render() {
    const { isLoading } = this.state;
    return (
      <>
        {isLoading && <Loader isLoading />}
        <section className="virtual-training">
          <Header title="Virtual Personal Training at Equinox" screenNumber={1} />
          <div className="custom-padding container">
            <div className="get-results">Get results. Wherever. WHenever.</div>
            <div className="vt-text">
              {'Get results wherever you can train. Your personal trainer will work with you to create a custom plan that’s tailored to your goals and guide you through workouts at home with our Virtual Personal Training by Equinox platform.'}
            </div>

            <div className="not-required-apply">
              <div className="not-required">Equinox membership not required.</div>
              <a className="terms-apply ml-lg-2" target="_blank" rel="noopener noreferrer" href="https://assets.cdn-equinox.com/pdf/Equinox+Virtual+Training+Terms.pdf">Terms apply</a>
            </div>
            <div className="vt-text pb-3">
              {'Together with your trainer you\'ll make results happen. It works like this:'}
            </div>
            <ul className="objectives-list">
              <li className="objectives-item">{'We\'ll find the perfect trainer for your goals'}</li>
              <li className="objectives-item">Your trainer will create a plan using what you have at home</li>
              <li className="objectives-item">{'You\'ll start training and working towards your goals'}</li>
            </ul>
            <button type="button" className="btn-purchase">
              <Link className="purchase-link" to="/nonMemberContact">
                {'Purchase Training Sessions'}
              </Link>
            </button>

            <button type="button" className="btn-eqx-member">
              <a href={AuthService.getLoginRedirectUrl()} className="eqx-link">Current Equinox Member</a>
            </button>
            <Footer />
          </div>
        </section>
      </>
    );
  }
}

export default VirtualTraining;
