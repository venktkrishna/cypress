import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <section className="sidebar">
        <div className="sidebar-header">
          <div className="sidebar-brand">VPT</div>
          <div className="sidebar-text">Amex offer</div>
        </div>
        <div>
          <div className="sidebar-list">
            <div className="sidebar-list-item">
              <div className="sidebar-menu">
                <Link className="menu-link" to="/virtualTraining">
                  {'Non-Member'}
                </Link>
              </div>
              <img src="images/svg/right-chevron.svg" className="arrow-right" alt="arrow-right" />
            </div>
            <div className="sidebar-list-item">
              <div className="sidebar-menu">Member – not PT client</div>
              <img src="images/svg/right-chevron.svg" className="arrow-right" alt="arrow-right" />
            </div>
            <div className="sidebar-list-item">
              <div className="sidebar-menu">Member – PT client</div>
              <img src="images/svg/right-chevron.svg" className="arrow-right" alt="arrow-right" />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Sidebar;
