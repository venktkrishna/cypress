import React from 'react';
import PropTypes from 'prop-types';

const Loader = ({
  children, isLoading, className
}) => {
  if (isLoading) {
    return (
      <div className={`loader__component ${className}`}>
        <img className="loader__image" src="images/svg/loader.svg" alt="loading..." />
      </div>
    );
  }
  return children;
};

Loader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node]),
  isLoading: PropTypes.bool.isRequired
};

Loader.defaultProps = {
  className: '',
  children: PropTypes.oneOfType([PropTypes.func, PropTypes.node])
};


export default Loader;
