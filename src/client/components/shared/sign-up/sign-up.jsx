import React, { Component } from 'react';
import Header from '../header/header';

class VirtualTraining extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <section className="signUp-section">
        <Header title="Virtual Personal Training at Equinox" screenNumber={2} />
        <div className="custom-padding">
          <div className="vt-text">
            {'Not an Equinox Member, not a problem. This offer is available exclusively for American Express Centurian and Platinum Card holders. No Equinox membership required.'}
          </div>

          <div className="info-title">CONTACT INFORMATION</div>
          <form className="sign-upForm">
            <div className="row">
              <div className="col-lg-5 col-md-5 col-sm-12">
                <input className="input-style" placeHolder="First Name" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5 col-md-5 col-sm-12">
                <input className="input-style" placeHolder="Last Name" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5 col-md-5 col-sm-12">
                <input className="input-style" type="email" placeHolder="Email" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-5 col-md-5 col-sm-12">
                <input className="input-style" type="telephone" placeHolder="Phone Number" />
              </div>
            </div>
          </form>

          <div className="info-title">MAILING INFORMATION</div>
          <form className="sign-upForm">
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <input className="input-style" placeHolder="Address Line 1" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <input className="input-style" placeHolder="Address Line 2" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <input className="input-style" placeHolder="City" />
              </div>
            </div>

            <div className="row">
              <div className="col-lg-3 col-md-3 col-6">
                <div className="dropdown">
                  <button className="drp-state" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                    {'State'}
                    <img className="arrow-down" src="images/svg/down-chevron-white.svg" alt="chevrom-down" />
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="/#">Abc</a>
                    <a className="dropdown-item" href="/#">Pqr</a>
                    <a className="dropdown-item" href="/#">Lmn</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-6">
                <input type="number" className="input-style" placeHolder="Zip Code" />
              </div>
            </div>
          </form>

          <div className="agree-text">
            {'By continuing, I agree to share my billing and contact information with a Membership Advisor.'}
          </div>
          <a target="_blank" href="https://www.equinox.com/memberpolicy" className="vt-link">Terms Apply</a>
          <div>
            <button type="button" className="btn-next">Next</button>
          </div>
          <div className="vt-text">Already an Equinox Member?</div>
          <a href="#/" className="vt-link">Sign in here</a>
          <div className="vt-footer">
            <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
            <p className="footer-text">
              <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
              <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
            </p>
            <p className="footer-text mb-0">
              <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default VirtualTraining;
