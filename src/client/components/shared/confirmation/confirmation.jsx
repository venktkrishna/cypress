import React, { Component } from 'react';
import Header from '../header/header';
import Footer from '../footer/footer';

class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <section className="confirmation">
        <Header title={"We'll take it from here"} screenNumber={3} />
        <div className="custom-padding container">
          <div className="confirmation-content">
            <div className="vt-text">
              {
                'Thank you for your purchase. A member of our Personal Training team will be in touch with you soon.'
              }
            </div>
            <div className="vt-text">
              Questions? Contact
              <a href="mailto:concierge@equinox.com"> concierge@equinox.com</a>
            </div>
          </div>
          <Footer />
        </div>
      </section>
    );
  }
}

export default Confirmation;
