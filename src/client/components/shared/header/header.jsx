import React from 'react';
import PropTypes from 'prop-types';
import withScreenDimensions from '../screen-dimension';

const headerImages = {
  1: 'images/vpt-img-1.jpg',
  2: 'images/vpt-img-2.jpg',
  3: 'images/vpt-img-3.jpg'
};

const headerDesktopImage = {
  1: 'images/desktop-images/vpt-img-1.jpg',
  2: 'images/desktop-images/vpt-img-2.jpg',
  3: 'images/desktop-images/vpt-img-3.jpg'
};

const Header = ({ title, screenNumber, isDesktop }) => (
  <div className="header-section">
    <img className="vt-headerImg" src={isDesktop ? headerDesktopImage[screenNumber] : headerImages[screenNumber]} alt="" />
    <img className="eqx-logo" src="images/svg/equinox-logo.png" alt="logo" />
    <p className="header-title">{title}</p>
  </div>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  screenNumber: PropTypes.number.isRequired,
  isDesktop: PropTypes.bool.isRequired
};

export default withScreenDimensions(Header);
