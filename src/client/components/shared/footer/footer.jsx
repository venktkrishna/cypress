import React from 'react';

const Footer = () => (
  <div className="vt-footer">
    <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
    <div className="footer-links">
      <p className="footer-text">
        <a target="_blank" rel="noopener noreferrer" href="https://www.equinox.com/terms">Terms and Conditions</a>
        <a className="ml-3" target="_blank" rel="noopener noreferrer" href="https://www.equinox.com/privacy">Privacy Policy</a>
      </p>
      <p className="footer-text mb-0 ml-lg-3">
        <a target="_blank" rel="noopener noreferrer" href="https://assets.cdn-equinox.com/pdf/Equinox+Virtual+Training+Terms.pdf">Virtual Training Terms</a>
      </p>
    </div>
  </div>
);

export default Footer;
