import React, { Component } from 'react';
import { debounce } from 'lodash';

export default function withScreenDimensions(WrappedComponent) {
  return class ScreenDimensions extends Component {
    state = {
      width: 0,
      height: 0,
      isMobile: false,
      isTablet: false,
      isDesktop: false
    };

    componentDidMount() {
      this.updateWindowDimensions();
      window.addEventListener(
        'resize',
        debounce(this.updateWindowDimensions, 100)
      );
    }

    componentWillUnmount() {
      window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
      const isMobile = window.innerWidth <= 650;
      const isTablet = window.innerWidth >= 651 && window.innerWidth <= 1024;
      const isDesktop = window.innerWidth >= 1025;
      this.setState({
        width: window.innerWidth,
        height: window.innerHeight,
        isMobile,
        isTablet,
        isDesktop
      });
    };

    render() {
      const {
        width, height, isMobile, isTablet, isDesktop
      } = this.state;
      return (
        <WrappedComponent
          {...this.props}
          windowWidth={width}
          windowHeight={height}
          isMobile={isMobile}
          isTablet={isTablet}
          isDesktop={isDesktop}
        />
      );
    }
  };
}
