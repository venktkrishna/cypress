import React from 'react';

/* istanbul ignore next */
const Header = () => (
  <header className="header__container">
    <span className="header__hamburger" />
    <span className="header__vertical-divider" />
    <h3 className="header__title">Amex VPT</h3>
    <button type="button" className="header__search__icon" onClick={() => { }} />
  </header>
);

export default Header;
