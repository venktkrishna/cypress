import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import classnames from 'classnames';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
import { Cookies, withCookies } from 'react-cookie';
import { REGEX } from '../../../../../config/app-config';
import Header from '../../shared/header/header';
import setUrl from '../../../utilites/utils';
import states from '../../../utilites/states';
import AuthService from '../../../utilites/auth-services';
import Footer from '../../shared/footer/footer';

const { EMAIL /* PHONE_NUMBER, ZIP_CODE */} = REGEX;

const NonMemberContact = (props) => {
  const {
    register, handleSubmit, errors, setValue
  } = useForm();
  const { cookies } = props;
  const [zipCode, updateZipCode] = useState('');
  const [phoneNumber, updatephoneNumber] = useState('');
  const [firstdropdown, setFirstdropdown] = useState('');

  const setPaymentCookies = (data) => {
    const cookieOptions = {
      path: '/',
      maxAge: 1200000,
      domain: window.location.origin.includes('localhost') ? 'localhost' : '.equinox.com',
      secure: !window.location.origin.includes('localhost')
    };
    cookies.set(
      'referrer',
      `${window.location.origin}/${window.location.path}`,
      cookieOptions
    );
    cookies.set('amexData', data, cookieOptions);
    window.location.href = window.location.origin.includes('localhost') ? 'http://localhost:30001' : setUrl('payment.equinox.com');
  };
  //* *********** common Function *******/
  const allowNumbers = (e) => {
    const pattern = new RegExp(/^[0-9\b]+$/);
    return pattern.test(e);
  };

  const onSubmit = (data) => {
    setPaymentCookies(data);
  };
  const handleChange = ({ target }) => {
    const { name, value } = target;
    if (name === 'zipCode' && value.length <= 6 && (allowNumbers(value) || value === '')) {
      updateZipCode(value);
      setValue('zipCode', value);
    }
    if (name === 'phoneNumber' && value.length <= 10 && (allowNumbers(value) || value === '')) {
      setValue('phoneNumber', value);
      updatephoneNumber(value);
    }
  };
  useEffect(() => {
    register({ name: 'zipCode' }, { required: true, validate: value => value.length >= 5 });
    register({ name: 'phoneNumber' }, { required: true, validate: value => value.length === 10 });
  }, []);

  const firstNameError = classnames('input-style', { invalid: errors.firstName && errors.firstName.type === 'required' });
  const lastNameError = classnames('input-style', { invalid: errors.lastName && errors.lastName.type === 'required' });
  const emailError = classnames('input-style', { invalid: errors.email && errors.email.type === 'required' });
  const phoneNumberError = classnames('input-style', { invalid: (errors.phoneNumber && errors.phoneNumber.type === 'required') || (phoneNumber ? phoneNumber.length < 10 : errors.phoneNumber) });
  const address1Error = classnames('input-style', { invalid: errors.address1 && errors.address1.type === 'required' });
  const address2Error = classnames('input-style', { invalid: errors.address2 && errors.address2.type === 'required' });
  const cityError = classnames('input-style', { invalid: errors.city && errors.city.type === 'required' });
  const stateError = classnames('input-style', { invalid: errors.state && errors.state.type === 'required' });
  const zipCodeError = classnames('input-style', { invalid: (errors.zipCode && errors.zipCode.type === 'required') || (zipCode ? zipCode.length < 5 : errors.zipCode) });

  return (
    <section className="non-pt-payment">
      <Header title="Virtual personal training at equinox" screenNumber={2} />
      <div className="custom-padding container">
        <div className="vt-text">
          {'Not an Equinox member? Not a problem. Please fill out your contact information to get started with Virtual Personal Training.'}
        </div>
        <div className="signin-text">
          {'Already an Equinox Member?'}
          <a className="signIn-link ml-lg-2" href={AuthService.getLoginRedirectUrl()}>Sign in here</a>
        </div>

        <div className="info-title">Contact Information</div>
        <form>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-12">

              <label className="input-container px-0" htmlFor="firstName">
                <input
                  id="firstName"
                  className={firstNameError}
                  placeHolder=" "
                  name="firstName"
                  ref={register({ required: true, validate: value => value.length > 1 })}
                />
                <span className="input-label" htmlFor="Option1">First Name</span>
                {errors.firstName && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>

              <label className="input-container px-0" htmlFor="lastName">
                <input
                  id="lastName"
                  className={lastNameError}
                  placeHolder=" "
                  name="lastName"
                  ref={register({ required: true, validate: value => value.length > 1 })}
                />
                <span className="input-label">Last Name</span>
                {errors.lastName && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>

              <label className="input-container px-0" htmlFor="email">
                <input
                  id="email"
                  className={emailError}
                  placeHolder=" "
                  name="email"
                  ref={register({ required: true, pattern: { value: EMAIL } })}
                />
                <span className="input-label">Email</span>
                {errors.email && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>

              <label className="input-container px-0" htmlFor="phoneNo">
                <input
                  id="phoneNo"
                  type="text"
                  className={(phoneNumber && phoneNumber.length > 9) ? 'input-style' : phoneNumberError}
                  placeholder=" "
                  onChange={handleChange}
                  value={phoneNumber}
                  onKeyDown={evt => ['e', 'E', '+', '-', '.'].includes(evt.key) && evt.preventDefault()}
                  name="phoneNumber"
                />
                <span className="input-label">Phone Number</span>
                {(phoneNumber ? phoneNumber.length < 10 : errors.phoneNumber) && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}

              </label>
            </div>
          </div>

          <div className="info-title">Mailing Address</div>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-12">
              <label className="input-container px-0" htmlFor="address1">
                <input
                  id="address1"
                  className={address1Error}
                  placeHolder=" "
                  name="address1"
                  ref={register({ required: true })}
                />
                <span className="input-label">Address Line 1</span>
                {errors.address1 && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>
              <label className="input-container px-0" htmlFor="address2">
                <input
                  id="address2"
                  className={address2Error}
                  placeHolder=" "
                  name="address2"
                  ref={register}
                />
                <span className="input-label">Address Line 2</span>
                {errors.address2 && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>

              <label className="input-container px-0" htmlFor="city">
                <input
                  id="city"
                  className={cityError}
                  placeHolder=" "
                  name="city"
                  ref={register({ required: true })}
                />
                <span className="input-label">City</span>
                {errors.city && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3 col-6">
              <label className="input-container px-0" htmlFor="state1">
                <select className={stateError} name="state" id="state1" onChange={e => setFirstdropdown(e.target.value)} value={firstdropdown} ref={register({ required: true })}>
                  <option value="" className="select-choose">State</option>
                  {states.US.values.map(state => <option key={state} className="select-choose" value={state}>{state}</option>)}
                </select>
                {firstdropdown && firstdropdown !== 'State' && <span className="input-label">State</span>}
              </label>
            </div>
            <div className="col-lg-3 col-md-3 col-6">
              <label className="input-container px-0" htmlFor="zipCode">
                <input
                  value={zipCode}
                  id="zipCode"
                  type="text"
                  className={(zipCode && zipCode.length > 4) ? 'input-style' : zipCodeError}
                  placeholder=" "
                  name="zipCode"
                  onKeyDown={evt => ['e', 'E', '+', '-'].includes(evt.key) && evt.preventDefault()}
                  onChange={handleChange}
                />
                <span className="input-label" htmlFor="Option1">Zip Code</span>
                {(zipCode ? zipCode.length < 5 : errors.zipCode) && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
              </label>
            </div>
          </div>
        </form>
        <div>
          <button
            type="submit"
            className="btn-next"
            onClick={handleSubmit(data => onSubmit(data))}
          >
            {'Next'}
          </button>
        </div>
        <Footer />
      </div>
    </section>
  );
};

NonMemberContact.propTypes = {
  cookies: PropTypes.instanceOf(Cookies).isRequired
};

export default withCookies(NonMemberContact);
