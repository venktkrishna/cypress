import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../../shared/header/header';

class NonMemberPurchase extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <section className="non-member-purchase">
        <Header title="Confirm purchase" screenNumber={1} />

        <div className="custom-padding">
          <div className="sub-headingContainer">
            <h2 className="sub-heading">Virtual Personal Training-Exclusive Pack</h2>
          </div>
          <div className="session-info d-flex">
            <div className="session-time" e>Six 1-hour Sessions</div>
            <div className="session-cost">$780</div>
          </div>
          <div className="info-title">Contact Information</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">John</div>
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">Doe</div>
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">John.doe@email.com</div>
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">(555) 555-5555</div>
              </div>
            </div>
          </form>

          <div className="info-title">Mailing Address</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">1 Park Ave</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">Mezz.</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">NY</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">NY</div>
              </div>
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">11201</div>
              </div>
            </div>
            <div className="edit-field">Edit</div>
          </form>

          <div className="info-title">Card Information</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">AMEX 0000</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">00/00</div>
              </div>
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">***</div>
              </div>
            </div>
            <div className="edit-field">Edit</div>
          </form>

          <div className="info-title">Billing Address</div>
          <form>
            <div className="chk-box">
              <label className="container" htmlFor="Option1">
                <span className="check-text">Same a mailing address</span>
                <input className="mail-input" id="Option1" type="checkbox" value="" />
                <span className="checkmark" />
              </label>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">65 Atlantic Ave</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">Apt 10</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <div className="info-text">Brooklyn</div>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">NY</div>
              </div>
              <div className="col-lg-3 col-md-3 col-6 d-flex">
                <div className="info-text">11201</div>
              </div>
            </div>
            <div className="edit-field">Edit</div>
          </form>

          <div className="vt-text">
            {'After purchasing your Virtual Personal Training sessions, a member of our Personal Training team will be in touch to match you with the right trainer for your goals.'}
          </div>

          <div className="vt-terms">
            <div className="chk-box">
              <label className="container" htmlFor="Option3">
                <span className="terms-text">I have reviewed and agreed to the</span>
                <a className="signIn-link" target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
                <input className="mail-input" id="Option3" type="checkbox" value="" />
                <span className="checkmark" />
              </label>
            </div>
          </div>

          <button type="button" className="btn-next">
            <Link className="btn-link" to="/confirmation">
              {'Purchase'}
            </Link>
          </button>

          <div className="vt-footer">
            <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
            <p className="footer-text">
              <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
              <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
            </p>
            <p className="footer-text mb-0">
              <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
            </p>
          </div>
        </div>

      </section>
    );
  }
}

export default NonMemberPurchase;
