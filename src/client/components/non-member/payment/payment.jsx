import React from 'react';
import classnames from 'classnames';
import { useForm } from 'react-hook-form';
// import { Link } from 'react-router-dom';
import { REGEX } from '@config/app-config';
import Header from '../../shared/header/header';

const {
  CARD, CARD_EXPIRATION, CARD_CVV, ZIP_CODE
} = REGEX;

const Payment = () => {
  const { register, handleSubmit, errors } = useForm();

  const onSubmit = (data) => {
    console.log('asdf', data);
  };

  const cardError = classnames('input-style', { 'input-box-error': errors.card && errors.card.type === 'required' });
  const expirationError = classnames('input-style', { 'input-box-error': errors.expiration && errors.expiration.type === 'required' });
  const CVVError = classnames('input-style', { 'input-box-error': errors.cvv && errors.cvv.type === 'required' });
  const address1Error = classnames('input-style', { 'input-box-error': errors.address1 && errors.address1.type === 'required' });
  const address2Error = classnames('input-style', { 'input-box-error': errors.address2 && errors.address2.type === 'required' });
  const cityError = classnames('input-style', { 'input-box-error': errors.city && errors.city.type === 'required' });
  const zipCodeError = classnames('input-style', { 'input-box-error': errors.zipCode && errors.zipCode.type === 'required' });

  return (
    <section className="payment">
      <Header title="Virtual Personal Training at Equinox" screenNumber={1} />

      <div className="custom-padding">
        <div className="sub-headingContainer">
          <h2 className="sub-heading">Virtual Personal Training-Exclusive Pack</h2>
        </div>
        <div className="session-info d-flex">
          <div className="session-time" e>Six 1-hour Sessions</div>
          <div className="session-cost">$780</div>
        </div>

        <div className="info-title">Card information</div>
        <form>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-12">
              {errors.card && (<span className="input-label input-error" htmlFor="Option1">Card number</span>)}
              <input
                className={cardError}
                placeHolder="Card number"
                name="card"
                ref={register({ required: true, pattern: { value: CARD } })}
              />
              {errors.card && (<span className="error-text">Please enter an American Express® card for purchase</span>)}
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3 col-6">
              <span className="input-label " htmlFor="Option1">Expiration</span>
              <input
                className={expirationError}
                placeHolder="Expiration"
                name="expiration"
                ref={register({ required: true, pattern: { value: CARD_EXPIRATION } })}
              />
            </div>
            <div className="col-lg-3 col-md-3 col-6">
              <span className="input-label" htmlFor="Option1">CVV</span>
              <input
                type="number"
                className={CVVError}
                placeHolder="CVV"
                name="cvv"
                ref={register({ required: true, pattern: { value: CARD_CVV } })}
              />
            </div>
          </div>

          <div className="info-title">Billing Address</div>
          <div className="chk-box">
            <label className="container" htmlFor="Option1">
              <span className="check-text">Same a mailing address</span>
              <input className="mail-input" id="Option1" type="checkbox" value="" />
              <span className="checkmark" />
            </label>
          </div>
          <div className="row">
            <div className="col-lg-6 col-md-6 col-12">
              {errors.address1 && (<span className="input-label input-error" htmlFor="Option1">Address Line 1</span>)}
              <input
                className={address1Error}
                placeHolder="Address Line 1"
                name="address1"
                ref={register({ required: true })}
              />
              {errors.address1 && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
            </div>
            <div className="col-lg-6 col-md-6 col-12">
              {errors.address2 && (<span className="input-label input-error" htmlFor="Option1">Address Line 2</span>)}
              <input
                className={address2Error}
                placeHolder="Address Line 2"
                name="address2"
                ref={register({ required: true })}
              />
              {errors.address2 && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
            </div>
            <div className="col-lg-6 col-md-6 col-12">
              {errors.city && (<span className="input-label input-error" htmlFor="Option1">City</span>)}
              <input
                className={cityError}
                placeHolder="City"
                name="city"
                ref={register({ required: true })}
              />
              {errors.city && (<img className="error-icon" alt="img" src="images/svg/icon-error.svg" />)}
            </div>
          </div>
          <div className="row">
            <div className="col-lg-3 col-md-3 col-6">
              {errors.state && (<span className="input-label input-error" htmlFor="Option1">State</span>)}
              <div className="dropdown">
                <button className="drp-state" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                  {'State'}
                  <img className="arrow-down" src="images/svg/down-chevron-white.svg" alt="chevrom-down" />
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton" name="state" ref={register({ required: true })}>
                  <a className="dropdown-item" href="/#">Abc</a>
                  <a className="dropdown-item" href="/#">Pqr</a>
                  <a className="dropdown-item" href="/#">Lmn</a>
                </div>
              </div>
            </div>
            <div className="col-lg-3 col-md-3 col-6">
              {errors.zipCode && (<span className="input-label input-error" htmlFor="Option1">Zipcode</span>)}
              <input
                type="number"
                className={zipCodeError}
                placeHolder="Zipcode"
                name="zipCode"
                ref={register({ required: true, pattern: { value: ZIP_CODE } })}
              />
            </div>
          </div>
        </form>
        <div>
          <button type="button" className="btn-next disabled" onClick={handleSubmit(data => onSubmit(data))}>
            {/* <Link className="btn-link" to="/nonMemberPurchase"> */}
            {'Review'}
            {/* </Link> */}
          </button>
        </div>
        <div className="vt-footer">
          <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
          <p className="footer-text">
            <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
            <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
          </p>
          <p className="footer-text mb-0">
            <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
          </p>
        </div>

      </div>
    </section>
  );
};

export default Payment;
