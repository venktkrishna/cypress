import React, { Component } from 'react';
import Header from '../../shared/header/header';

class Ptpayment extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <section className="pt-payment">
        <Header title={'We\'ll take it from here'} screenNumber={1} />

        <div className="custom-padding">
          <div className="tier-trainer">You are currently working with a Tier 3 trainer.</div>
          <div className="row">
            <div className="col-12 col-md-7 col-lg-6">
              {/* <ul className="tier-list">
                <li className="tier-name">
                  {'Tier 2'}
                  <img className="down-chevron"
                   src="images/svg/down-chevron-white.svg" alt="down-chevron" />
                </li>
                <li className="tier-name">Tier 3</li>
                <li className="tier-name">Tier 3+</li>
                <li className="tier-name">Tier X</li>
              </ul> */}

              <div className="panel-group">
                <div className="panel panel-default">
                  <div className="panel-heading">
                    <h4 className="tier-name-btn">
                      <a className="tier-link" data-toggle="collapse" href="#collapse1">Tier 2</a>
                      <img className="down-chevron" src="images/svg/down-chevron-white.svg" alt="down-chevron" />
                    </h4>
                  </div>
                  <div id="collapse1" className="panel-collapse collapse">
                    <ul className="list-unstyled tier-list">
                      <li className="tier-name">Tier 3</li>
                      <li className="tier-name">Tier 3+</li>
                      <li className="tier-name">Tier X</li>
                    </ul>
                  </div>
                </div>
              </div>

            </div>
          </div>
          <div className="row">
            <div className="col-12 col-md-7 col-lg-6">
              <ul className="list-unstyled sessions-list">
                <li className="sessions-content">
                  <label className="radio-container" htmlFor="session1">

                    <input id="session1" type="radio" name="radio" />
                    <span className="session-name">Virtual Personal Training - Exclusive Pack</span>
                    <span className="session-count">6 sessions (1-hour)</span>
                    <span className="radio-checkmark" />
                  </label>
                  <div className="session-charges">$780</div>
                </li>
                <li className="sessions-content">
                  <label className="radio-container" htmlFor="session2">
                    <span className="session-name">12 sessions (1-hour)</span>
                    <input id="session2" type="radio" checked="checked" name="radio" />
                    <span className="radio-checkmark" />
                  </label>
                  <div className="session-charges">$1440</div>
                </li>
                <li className="sessions-content">
                  <label className="radio-container" htmlFor="session3">
                    <span className="session-count">24 sessions (1-hour)</span>
                    <input id="session3" type="radio" checked="checked" name="radio" />
                    <span className="radio-checkmark" />
                  </label>
                  <div className="session-charges">$2760</div>
                </li>
                <li className="sessions-content">
                  <label className="radio-container" htmlFor="session4">
                    <span className="session-count">48 sessions (1-hour)</span>
                    <input id="session4" type="radio" checked="checked" name="radio" />
                    <span className="radio-checkmark" />
                  </label>
                  <div className="session-charges">$5280</div>
                </li>
              </ul>
            </div>
          </div>

          <div className="vt-note">* Pricing for this promotion may vary from your usual 6-pack pricing</div>

          <div className="contact-info">
            <div className="info-title">Contact Information</div>
            <div className="row contact-details">
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">John</div></div>
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">Doe</div></div>
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">John.doe@email.com</div></div>
            </div>
          </div>

          <div className="info-title">card information</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label input-error" htmlFor="Option1">Card number</span>
                <input className="input-style input-box-error" placeHolder="Card number" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label " htmlFor="Option1">Expiration</span>
                <input className="input-style" placeHolder="Expiration" />
              </div>
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">CVV</span>
                <input type="number" className="input-style" placeHolder="CVV" />
              </div>
            </div>
          </form>

          <div className="info-title">Billing Address</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label" htmlFor="Option1">Address Line 1</span>
                <input className="input-style" placeHolder="Address Line 1" />
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label " htmlFor="Option1">Address Line 2</span>
                <input className="input-style" placeHolder="Address Line 2" />
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label " htmlFor="Option1">City</span>
                <input className="input-style" placeHolder="City" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">State</span>
                <div className="dropdown">
                  <button className="drp-state" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                    {'State'}
                    <img className="arrow-down" src="images/svg/down-chevron-white.svg" alt="chevrom-down" />
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="/#">Abc</a>
                    <a className="dropdown-item" href="/#">Pqr</a>
                    <a className="dropdown-item" href="/#">Lmn</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">Zipcode</span>
                <input type="number" className="input-style" placeHolder="Zipcode" />
              </div>
            </div>
          </form>
          <div>
            <button type="button" className="btn-next">Review</button>
          </div>

          <div className="vt-footer">
            <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
            <p className="footer-text">
              <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
              <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
            </p>
            <p className="footer-text mb-0">
              <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default Ptpayment;
