// Default named exports for all the components here
export { default as Header } from './header/header-component';
export { default as Footer } from './footer/footer-component';

// Shared
export { default as Sidebar } from './shared/sidebar/sidebar';
export { default as VirtualTraining } from './shared/virtual-training/virtual-training';
export { default as Login } from './shared/sign-in/login';
export { default as SignUp } from './shared/sign-up/sign-up';
export { default as Confirmation } from './shared/confirmation/confirmation';

// PT_member
export { default as PtPayment } from './PT-member/payment/payment';
export { default as MemberPtPurchase } from './PT-member/purchase/purchase';

// PT-Non-Member
export { default as NonPtPayment } from './PT-non-member/payment/payment';
export { default as NonPTPurchase } from './PT-non-member/purchase/purchase';

// Non-Member
export { default as NonMemberPurchase } from './non-member/purchase/confirm-purchase';
export { default as NonMemberContact } from './non-member/contact-info/contact-info';
export { default as NonMemberPayment } from './non-member/payment/payment';
