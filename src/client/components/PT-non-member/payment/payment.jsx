import React, { Component } from 'react';
import Header from '../../shared/header/header';

class NonPtpayment extends Component {
  constructor(props) {
    super(props);

    this.state = {

    };
  }

  render() {
    return (
      <section className="non-pt-payment">
        <Header title="Virtual Personal Training at Equinox" screenNumber={1} />

        <div className="custom-padding">
          <div className="sub-headingContainer">
            <h2 className="sub-heading">Virtual Personal Training-Exclusive Pack</h2>
          </div>
          <div className="session-info d-flex">
            <div className="session-time" e>Six 1-hour Sessions</div>
            <div className="session-cost">$780</div>
          </div>
          <div className="contact-info">
            <div className="info-title">Contact Information</div>
            <div className="row contact-details">
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">John</div></div>
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">Doe</div></div>
              <div className="col-lg-6 col-md-6 col-12"><div className="info-text">John.doe@email.com</div></div>
            </div>
          </div>

          <div className="info-title">card information</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label input-error" htmlFor="Option1">Card number</span>
                <input className="input-style input-box-error" placeHolder="Card number" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label " htmlFor="Option1">Expiration</span>
                <input className="input-style" placeHolder="Expiration" />
              </div>
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">CVV</span>
                <input type="number" className="input-style" placeHolder="CVV" />
              </div>
            </div>
          </form>

          <div className="info-title">Billing Address</div>
          <form>
            <div className="row">
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label" htmlFor="Option1">Address Line 1</span>
                <input className="input-style" placeHolder="Address Line 1" />
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label " htmlFor="Option1">Address Line 2</span>
                <input className="input-style" placeHolder="Address Line 2" />
              </div>
              <div className="col-lg-6 col-md-6 col-12">
                <span className="input-label " htmlFor="Option1">City</span>
                <input className="input-style" placeHolder="City" />
              </div>
            </div>
            <div className="row">
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">State</span>
                <div className="dropdown">
                  <button className="drp-state" type="button" id="dropdownMenuButton" data-toggle="dropdown">
                    {'State'}
                    <img className="arrow-down" src="images/svg/down-chevron-white.svg" alt="chevrom-down" />
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a className="dropdown-item" href="/#">Abc</a>
                    <a className="dropdown-item" href="/#">Pqr</a>
                    <a className="dropdown-item" href="/#">Lmn</a>
                  </div>
                </div>
              </div>
              <div className="col-lg-3 col-md-3 col-6">
                <span className="input-label" htmlFor="Option1">Zipcode</span>
                <input type="number" className="input-style" placeHolder="Zipcode" />
              </div>
            </div>
          </form>
          <div>
            <button type="button" className="btn-next">Review</button>
          </div>
          <div className="vt-footer">
            <p className="">Copyright © 2020 Equinox Holdings, Inc.</p>
            <p className="footer-text">
              <a target="_blank" href="https://www.equinox.com/terms">Terms and Conditions</a>
              <a className="ml-3" target="_blank" href="https://www.equinox.com/privacy">Privacy Policy</a>
            </p>
            <p className="footer-text mb-0">
              <a target="_blank" href="https://www.equinox.com/memberpolicy">Virtual Training Terms</a>
            </p>
          </div>
        </div>
      </section>
    );
  }
}

export default NonPtpayment;
