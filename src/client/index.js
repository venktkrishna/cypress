import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './app';
import configureStore from './state/store';
import styles from './app.scss';
import 'bootstrap';
require('bootstrap/dist/css/bootstrap.css');
const store = configureStore();
import AuthService from '../client/utilites/auth-services'
const ApplicationRoot = document.getElementById('app-root');

/* istanbul ignore else */
if (ApplicationRoot) {
  ReactDOM.render(
    <Provider store={store}>
      <App />
    </Provider>,
    ApplicationRoot
  );
} else {
  console.error('React: Unable to mount application');
}

if (process.env.NODE_ENV === 'development') {
  module.hot.accept();
}
