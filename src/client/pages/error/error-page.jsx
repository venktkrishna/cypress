import React from 'react';
import { Link } from 'react-router-dom';
// import { getCachedUser } from '@config/adal-config';
// import BasePage from '../base/base-page';

const ErrorPage = () => (
  // const user = getCachedUser();
  // const isLoggedIn = user && Object.keys(user).length;
  <div className="error-page">
    <h2>404: Not found</h2>
    <Link to="reservations">Goto Home Page</Link>
  </div>
);

export default ErrorPage;
