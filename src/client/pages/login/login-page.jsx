import React from 'react';
import BasePage from '@BasePage';

export default function LoginPage() {
  return (
    <BasePage
      includeHeader={false}
      includeFooter={false}
    >
      <div className="login__page">
        <div className="row--flex">
          <div className="login__content">
            <div className="login__content--padding">
              <span className="login__eqx__logo" />
              <h2 className="login__title">
                {'Amex VPT'}
              </h2>
              <button
                id="Outlook"
                className="login__button"
                title="Log in using your Outlook account"
                type="submit"
              >
                <span className="login__button__icon" />
                {'Sign in with Outlook'}
              </button>
              <h6 className="login__cta">
                {'Please use your Outlook'}
                <br />
                {'account to sign in.'}
              </h6>
            </div>
          </div>
        </div>
      </div>
    </BasePage>
  );
}
