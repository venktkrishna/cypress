import React from 'react';
import PropTypes from 'prop-types';

import { Header, Footer } from '@components';
import ErrorBoundary from '@ErrorBoundary';

export default function BasePage(props) {
  const { children, includeHeader, includeFooter } = props;
  return (
    <ErrorBoundary>
      <div className="app-container">
        {includeHeader && <Header />}
        <div className="content-container">
          {children}
        </div>
        {includeFooter && <Footer />}
      </div>
    </ErrorBoundary>
  );
}

BasePage.propTypes = {
  children: PropTypes.node,
  includeHeader: PropTypes.bool,
  includeFooter: PropTypes.bool
};

BasePage.defaultProps = {
  children: [],
  includeHeader: true,
  includeFooter: true
};
