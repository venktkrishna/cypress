/* eslint-disable no-console */
const express = require('express');
const path = require('path');

const app = express();
const { HOST } = require('../../config/app-config');

app.use(express.static('dist/public'));

app.get('/login-test', (req, res) => res.send('login'));

app.get('/*', (req, res) => {
  res.sendFile('index.html', { root: 'dist/public/' });
});

// eslint-disable-next-line no-unused-vars
app.use((err, req, res, next) => {
  console.log(err.stack);
  res.status(500).send(`${err.stack} : ${__dirname} : ${path.dirname(__filename)}`);
});

app.listen(HOST === 'localhost' ? 8888 : 8080);
