module.exports = {
  "extends": "airbnb",
  "parser": "babel-eslint",
  "plugins": [
    "filenames"
  ],
  "env": {
    "node": true,
    "es6": true,
    "browser": true
  },
  "rules": {
    "indent": [2, 2],
    "linebreak-style": 0,
    "comma-dangle": [2, "never"],
    "filenames/match-regex": [2, "^[a-z0-9]*(?:[a-z0-9+-])+[a-z0-9]+$", true],
    "react/no-danger": true,
    "jsx-a11y/label-has-associated-control": ["error", {
      "required": {
        "some": ["nesting", "id"]
      }
    }],
    "jsx-a11y/label-has-for": ["error", {
      "required": {
        "some": ["nesting", "id"]
      }
    }],
    "react/jsx-no-target-blank": [false, { "allowReferrer": true, "enforceDynamicLinks": 'never' }]
  },
  "settings": {
    "import/resolver": {
      "webpack": { 
        'config': './config/webpack/base.config.js'
      }
    }
  }
}