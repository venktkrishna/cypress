#!groovy
@Library('eqx-shared-libraries')
String version

def featureEnv = env.BRANCH_NAME != 'master'
String clustername = "apps-prod-linux"
String Servicename = "amexvpt-prod"
String artifactBucket = "JenkinsArtifacts"
String awsRegion = "us-east-1"
String appName = "amexvpt"
String dockerFilePath = "."
String TASK_FAMILY = "amexvpt"
String newRelicUri = "https://s3.amazonaws.com/JenkinsArtifacts/newrelic/newrelic-netcore20-agent_8.8.83.0_amd64.deb"
String projectFile = "pom.xml"
String versionKey = "version"
String terraformDir = "eqx_apps/amexvpt"
String branchName = env.BRANCH_NAME
String env = env.BRANCH_NAME
String app_id = "https://api.newrelic.com/v2/applications/398458964/deployments.json"
String terraformPath = "eqx_apps/amexvpt"

String ecrRepoUri = featureEnv ? "813561490937.dkr.ecr.${awsRegion}.amazonaws.com/amexvpt_${env}" : "813561490937.dkr.ecr.${awsRegion}.amazonaws.com/amexvpt_master"

pipeline {
    agent none

    options {
        buildDiscarder(logRotator(numToKeepStr: '30'))
        disableConcurrentBuilds()
        timeout(time: 6, unit: 'HOURS')
        ansiColor('xterm')
    }

    stages {
        stage('Build version') {
            agent { label 'linux' }
            steps {
                script {
                    version = featureEnv ?
                            VersionNumber(versionNumberString:'${BUILD_YEAR}.${BUILD_MONTH}.${BUILD_DAY}.${BUILDS_TODAY}', skipFailedBuilds:false) :
                            VersionNumber(versionNumberString: '2.0.${BUILD_NUMBER, X}', skipFailedBuilds:false)
                    currentBuild.displayName = version
                    println "Pipeline Version='${version}'"
                }
            }
        }
		
 stage('Env Build') {
           agent { label 'linux-terraform' }
           steps {
              checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: '']], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'eq00svc-buildmaster-bitbucket-api-key', url: 'https://bitbucket.org/equinoxfitness/eqx-terraform']]])
              dir(terraformPath) {
                 sh("""
                    terraform init
                    echo '${appName}_version = "${version}"' > terraform.tfvars
                    terraform refresh
                    terraform output -json | jq -r '.[] | .value[]' | tr -d '{}' | sed 's/"//g' | sed  '/^\$/d' | sed 's/: /=/g' > env_vars.txt
                    echo 'GIT_BRANCH=${GIT_BRANCH}' >> env_vars.txt
                    echo 'GIT_COMMIT=${GIT_COMMIT}' >> env_vars.txt
                    echo 'BUILD_DISPLAY_NAME=${BUILD_DISPLAY_NAME}' >> env_vars.txt
                    cat env_vars.txt
                 """)
                 stash includes: "env_vars.txt", name: "${appName}-docker"
              }
           }
        }		
		
		

        /*stage('Code Analytics') {
        
            agent { label 'linux-maven' }
            tools {
                maven 'maven-3.5.0'
            }
            environment{
                // https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner
                SONAR_SCANNER_OPTS="-Xmx512m"
            }
            steps {
                dir("${WORKSPACE}") {
                    unstash name: "${appName}-build-output-${env}"
                    withSonarQubeEnv('Sonarqube') {
                        mavenTest(projectFile, branchName)
                    }
                }

            }
        }*/		

        stage('Artifact Build') {
            when {
                anyOf { branch 'develop'; branch 'master'; branch 'release' }
            }		
            agent { label 'linux-docker' }
            steps {
                checkout scm
                unstash name: "${appName}-docker"
                dir(dockerFilePath) {
                     sh("""
                        while IFS= read -r line; do 
                            build_args+=" --build-arg \$line" 
                        done < "env_vars.txt"
                        #echo \$build_args
                        docker build -t ${ecrRepoUri}:${version} \$build_args --no-cache=true .
                        eval \$(aws ecr get-login --no-include-email --region ${awsRegion})
                        docker push ${ecrRepoUri}:${version}
                        docker rmi ${ecrRepoUri}:${version}
                     """)
               }
            }
        }
		
        stage('Int Deploy') {
			 when {
            branch "develop"
			}
            agent { label 'linux-terraform' }
            steps {
                terraformAppDeploy(version, appName, "int", terraformDir, versionKey)
            }
        }
        
        stage('Test Approval'){
			 when {
            branch "develop"
			}
            agent none
            steps{
                timeout(time: 1, unit: 'DAYS') {
                    input('Deploy to Test?')
                }
            }
        }
        stage('Test Deploy') {
			 when {
            branch "develop"
			}
            agent { label 'linux-terraform' }
            steps {
                terraformAppDeploy(version, appName, "test", terraformDir, versionKey)
            }
        }

        stage('Staging Deploy') {
			 when {
            branch "release"
			}
            agent { label 'linux-terraform' }
            steps {
               terraformAppDeploy(version, appName, "stag", terraformDir, versionKey)
            }
        }
		
        stage('Preprod Deploy') {
			 when {
            branch "master"
			}
            agent { label 'linux-terraform' }
            steps {
               terraformAppDeploy(version, appName, "preprod", terraformDir, versionKey)
            }
        }
		
        
        stage('Prod Approval'){
			 when {
            branch "master"
			}
            agent none
            steps{
                timeout(time: 1, unit: 'DAYS') {
                    input('Deploy to Prod?')
                }
            }
        }

        stage('Prod Deploy') {
			 when {
            branch "master"
			}
            agent { label 'linux-terraform' }
            steps {
               terraformAppDeploy(version, appName, "prod", terraformDir, versionKey)
            }
        }
		
		stage('Deployment Verification') {
            when {
                branch "master"
            }
			agent { label 'linux-terraform' }
			steps {
				dockerversion(clustername, Servicename, awsRegion)
			}
		} 	
		
		stage('Git Push To Origin') {
            when {
                branch "master"
            }
            agent { label 'linux' }
			steps {

				withCredentials([usernamePassword(credentialsId: 'eq00svc-buildmaster-bitbucket-api-key', passwordVariable: 'GIT_PASS', usernameVariable: 'GIT_USER')]) {
                sh "git tag ${version}"
                sh "git push https://${GIT_USER}:${GIT_PASS}@bitbucket.org/equinoxfitness/amex-vpt-application.git ${version}"
				}
			}
		}		
		
    }
}
