const webpack = require('webpack');
const merge = require('webpack-merge');

const baseConfig = require('./base.config.js');

module.exports = merge(baseConfig, {
  module: {
    rules: [
      {
        test: /\.json$/,
        exclude: /node_modules/,
        use: 'json-loader'
      },
      {
        test: /\.(scss)$/,
        use: ['style-loader', 'css-loader?sourceMap', 'sass-loader?sourceMap']
      }
    ]
  },

  devtool: 'cheap-module-eval-source-map',

  devServer: {
    contentBase: './dist',
    hot: true,
    open: true,
    historyApiFallback: true,
    host: 'localhost',
    port: '8080',
    https: false,
    hotOnly: false,
    disableHostCheck: true
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV) || JSON.stringify('development'),
    })
  ],

  node: {
    fs: 'empty'
  }
});
