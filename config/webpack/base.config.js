const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const GitRevisionPlugin = require('git-revision-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const CircularDependencyPlugin = require('circular-dependency-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    main: path.resolve(__dirname, '../../src/client/')
  },

  output: {
    path: path.resolve(__dirname, '../../dist/public/'),
    filename: '[name].[hash].js',
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: ['babel-loader', 'eslint-loader', 'source-map-loader'],
        exclude: /node_modules/,
        enforce: 'pre'
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf|svg|png|jpg|gif|css)$/,
        use: ['file-loader']
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default']
    }),
    new CaseSensitivePathsPlugin(),
    new CircularDependencyPlugin(),
    new CleanWebpackPlugin(),
    new CopyPlugin([
      {
        from: './src/client/static',
        to: './',
        ignore: ['.DS_Store']
      }
    ]),
    new HtmlWebpackPlugin({
      title: 'AMEX-VPT',
      template: './src/client/static/index.html',
      filename: 'index.html'
    }),
    new Dotenv({
      systemvars: true,
      expand: true,
      path: '.env'
    }),
    new webpack.DefinePlugin({
      'process.env': JSON.stringify(process.env)
    })
  ],
  resolve: {
    alias: {
      '@api': path.resolve(__dirname, '../api-config.js'),
      '@config': path.resolve(__dirname, '../../config'),
      '@scaffolding': path.resolve(__dirname, '../../src/client/scaffolding/'),
      '@components': path.resolve(__dirname, '../../src/client/components'),
      '@pages': path.resolve(__dirname, '../../src/client/pages'),
      '@redux': path.resolve(__dirname, '../../src/client/state/redux'),
      '@server': path.resolve(__dirname, '../../src/server'),
      '@ErrorBoundary': path.resolve(__dirname, '../../src/client/scaffolding/error-boundary/error-boundary'),
      '@BasePage': path.resolve(__dirname, '../../src/client/pages/base/base-page'),
    },
    extensions: ['*', '.js', '.jsx']
  }
};


