// App Config
module.exports = {
  PORT: process.env.PORT,
  HOST: process.env.HOST || 'localhost',
  __DEV__: process.env.NODE_ENV === "development",
  __TEST__: process.env.NODE_ENV === "test",
  REDUX_LOGGER_ENABLED: true,
  MOCK_MODE: 'success', // success | failure | false
  MOCK_RESPONSE_DELAY: 1, // (ms) // 1000
  API_ENDPOINTS: {
    QA: '<YOUR_API_GATEWAY_BASE_PATH>',
    STAG:  '<YOUR_API_GATEWAY_BASE_PATH>',
    PRODCUCTION:  '<YOUR_API_GATEWAY_BASE_PATH>',
  },
  REGEX: {
    NUMERIC: /\/([0-9]+)/,
    LEADING_SLASH: /^\/+/,
    ALL_SLASHES: /\//g,
    EMAIL: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
    PHONE_NUMBER: /^\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/,
    ZIP_CODE: /^\d{5}(?:[-\s]\d{4})?$/,
    CARD: /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/,
    CARD_EXPIRATION: /^(0[1-9]|1[0-2])\/?([0-9]{2})$/,
    CARD_CVV: /^[0-9]{3,4}$/
  }
};