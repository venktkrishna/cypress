import axios from 'axios';
import get from 'lodash.get';
import apiRequest from '@redux/api/actions';
import { configureMocks } from './mock-config';
import { getTimeSlots } from './api-config';
import {
  __DEV__,
  MOCK_MODE
} from './app-config';

// -----------------------------------------
// CORS - options (If not using local proxy)
// -----------------------------------------
// const { location } = window;
// const { protocol, hostname } = location;
// const apihostname = 'localhost'
// const BASE_API_URL = `${protocol}//${apihostname}`;
// -----------------------------------------
const client = axios.create({
  // timeout: 1000,
  headers: {
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Credentials': true,
    'Access-Control-Allow-Origin': window.location.origin // For CORS (if not using proxy)
  },
});

if (MOCK_MODE) {
  configureMocks(client);
} else {
  // Apply Auth Token if not using mock data
  client.interceptors.request.use(
    async (config) => {
      // add token  ----> getAppToken();
      if (token) config.headers.authorizationToken = `${token}`;
      return config;
    },
    (error) => (Promise.reject(error))
  );
}

export const dispatchErrorMessage = (error, failureAction, dispatch, isDefaultToast) => {
  const message = 'Oops it seems that something went wrong! Please try again in a few minutes.';
  if (isDefaultToast) {
    toast.error(message, {
      toastId: 'apiError'
    });
  }
  dispatch(failureAction(error));
};

/**
 * @param {string} url : url for request
 * @param {string} method : optional : GET, POST, UPDATE, DELETE. Default is GET
 * @param {string} data : optional: payload for POST, UPDATE, DELETE
 * @param {function} successAction : successAction after successful XHR request
 * @param {function} failureAction : failureAction after failure XHR request
 * @param {function} successCallback : optional : successCallback after successful XHR request
 */
export const asyncRequest = async ({
  url,
  method = 'GET',
  data = {},
  successAction,
  failureAction,
  successCallback,
  dispatch,
  serviceName,
  isDefaultToast = true
}) => {
  dispatch(apiRequest());
  let hasNetworkError = false;
  let apiError;
  const options = Object.assign({}, { url }, { method }, { data }, { serviceName });
  const response = await client.request(options)
    .catch((error) => {
      apiError = get(error, 'response.data');
      hasNetworkError = true;
    });

  if (hasNetworkError) {
    dispatchErrorMessage(apiError, failureAction, dispatch, isDefaultToast);
    return apiError;
  }

  if (successCallback) {
    successCallback(response.data);
  }

  if (successAction && (get(response, 'status') === 200 || get(response, 'status') === 201 || get(response, 'status') === 204)) {
    dispatch(successAction(response.data));
  }
  return response;
};