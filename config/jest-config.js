module.exports = {
  rootDir: './../',
  roots: ['<rootDir>/src', '<rootDir>/config'],
  setupFilesAfterEnv: [
    '<rootDir>/scripts/jest-setup.js'
  ],
  testResultsProcessor: '<rootDir>/node_modules/jest-html-reporter',
  reporters: [
    'default',
    ['<rootDir>/node_modules/jest-html-reporter', {
      pageTitle: 'Test Cases',
      outputPath: '../test-report/cases/index.html',
      includeFailureMsg: true,
      includeConsoleLog: true,
      executionMode: 'reporter',
      theme: 'lightTheme'
    }]
  ],
  collectCoverageFrom: [
    '**/*.{js,jsx}',
    '!node_modules/**'
  ],
  coverageDirectory: '<rootDir>/test-report/coverage',
  coverageThreshold: {
    global: {
      branches: 90,
      functions: 90,
      lines: 90,
      statements: 90
    }
  },
  testMatch: [
    '**/__tests__/**/*.{js,jsx}',
    '**/?(*-)(spec|test).{js,jsx}',
    '**/tests.js'
  ]
};