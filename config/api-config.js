import { API_ENDPOINTS } from './app-config';

export function getServicePath() {
  // #TODO: Handle ENV switching (QA, STAG, PROD)
  return API_ENDPOINTS.STAG;
}

// Example ENDPOINT
export const getTimeSlots = (facilityId) => (`${getServicePath()}/facility/${facilityId}/slots`);
