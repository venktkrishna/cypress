FROM node:10.15.3-alpine

# Install bash
# ------------------
RUN apk add --update bash && rm -rf /var/cache/apk/*

# ------------------
# Create app directory
# ------------------
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# ------------------
# ENV Vars
# ------------------
ENV HOST 0.0.0.0
ENV PORT 8080
ENV DOMAIN amexvpt

# ------------------
# GOOGLE ENV Vars
# ------------------
ARG GOOGLE_CLIENT_ID
ARG GOOGLE_CLIENT_SECRET

# ------------------
# AUTH TOKENS ENV Vars
# ------------------

# ------------------
# Setting args in ENV
# ------------------

# ------------------
# Install app dependencies
# ------------------
COPY package.json /usr/src/app/
RUN yarn install
COPY . ./ 
RUN yarn build

EXPOSE 8080:8080

CMD ["node", "./dist/server.js"]