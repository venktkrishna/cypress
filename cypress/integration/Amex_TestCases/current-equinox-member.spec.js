describe('Testing in Amex', () => {
    before(() => {        
        cy.visit('http://localhost:8080');    
    });

    it('Current Equinox Member Testing', () => {              
        // Header section
        cy.get('.header-title').should('have.text','Virtual Personal Training at Equinox');

        // Click on purchase training sessions
        cy.get('.btn-eqx-member').click();   
        cy.wait(2000);
        cy.get('.user-inputs').eq(0).type("290753@equinox.com");
        cy.get('.user-inputs').eq(1).type("equinox1");
        cy.get('.btn').click();
        cy.wait(20000);
    });  
});
