import { equal } from "assert";

describe('Purchase training sessions Testing', () => {
    before(() => {
        cy.visit('http://localhost:8080');
    });

    it('Contact Information', () => {
        cy.get('.header-title').should('have.text', 'Virtual Personal Training at Equinox');
        // Click on purchase training sessions
        cy.get('.purchase-link').click();
        // Click on Next Button to test validations
        cy.get('.btn-next').click();
        cy.get('.input-style.invalid').should('have.css', 'text-decoration', 'none solid rgb(255, 0, 0)');
        cy.get('.error-icon').should('have.css', 'text-decoration', 'none solid rgb(255, 255, 255)');

        // Form Filling: Contact Information
        cy.get('#firstName').type('Aaron');
        cy.get('#lastName').type('Abelson');
        cy.get('#email').type('aaron');
        cy.get('#email').should('have.css', 'text-decoration', 'none solid rgb(255, 255, 255)');
        cy.get('#email').type('aaron@gmail.com');
        cy.get('#phoneNo').type('8912298');
        cy.get('#phoneNo').should('have.css', 'text-decoration', 'none solid rgb(255, 0, 0)');
        cy.get('#phoneNo').type('9898912298');
    });
    it('Mailing Address', () => {
        // Click on Next Button to test validations
        cy.get('.btn-next').click();
        cy.get('.input-style.invalid').should('have.css', 'text-decoration', 'none solid rgb(255, 0, 0)');
        cy.get('.error-icon').should('have.css', 'text-decoration', 'none solid rgb(255, 255, 255)');
        // Form Filling: Mailing Address
        cy.get('#address1').type('US');
        cy.get('#address2').type('US');
        cy.get('#city').type('CITY');
        cy.get('#state1').select('MD');
        cy.get('#zipCode').type('1241');
        cy.get('#zipCode').should('have.css', 'text-decoration', 'none solid rgb(255, 0, 0)');
        cy.get('#zipCode').type('124156');
        cy.get('.btn-next').click();
    });
    it('Amex-payreview', () => {
        // pay review page:        
        cy.get(':nth-child(4) > .info-title').should('have.text', 'Card information');
        cy.get('.btn-next').click();
        cy.get('.input-style.invalid').should('have.css', 'text-decoration', 'none solid rgb(255, 0, 0)');                                   
        cy.getIframe('first-data-payment-field-name').find('#name').type('AARON ABELSON');                        
        cy.getIframe('first-data-payment-field-card').find('#card').type('345823233222453');   
        cy.getIframe('first-data-payment-field-exp').find('#exp').type('12/30');
        cy.getIframe('first-data-payment-field-cvv').find('#cvv').type('2312');                                   
        cy.get('.checkmark').click();
        cy.get('.col-lg-6 > :nth-child(1) > .input-style').type('Address');
        cy.get(':nth-child(2) > .input-style').type('Address2');
        cy.get(':nth-child(3) > .input-style').type('city');        
        cy.get(':nth-child(3) > :nth-child(1) > .input-container > .input-style').select('MI');
        cy.get(':nth-child(2) > .input-container > .input-style').type('123123');
        cy.get('.btn-next').click();
        cy.wait(8000);        
    });
    it('Confirm-Purchase', () => {
        cy.get('.edit-field').click();
        cy.wait(2000);
        cy.getIframe('first-data-payment-field-name').find('#name').type('AARON ABELSON');                        
        cy.getIframe('first-data-payment-field-card').find('#card').type('345823233222453');   
        cy.getIframe('first-data-payment-field-exp').find('#exp').type('12/30');
        cy.getIframe('first-data-payment-field-cvv').find('#cvv').type('2312');                                   
        cy.get('.checkmark').click();
        cy.get('.col-lg-6 > :nth-child(1) > .input-style').type('Address');
        cy.get(':nth-child(2) > .input-style').type('Address2');
        cy.get(':nth-child(3) > .input-style').type('city');        
        cy.get(':nth-child(3) > :nth-child(1) > .input-container > .input-style').select('MI');
        cy.get(':nth-child(2) > .input-container > .input-style').type('123123');
        cy.get('.btn-next').click();
        cy.wait(2000);
        cy.get('.btn-next').should('be.disabled');
        cy.get('.check-text').eq(0).click({force: true});        
        cy.get('.chk-subline > .check-text').click();
        cy.get('button').contains('Purchase').click();    
    });
});

